import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.text.html.ImageView;

public class ViewerFrame extends JFrame implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4874139949403542959L;
	@Override
	public void run() {
		// TODO Auto-generated method stub
		setFrameLayout();
        setVisible(true);
	}
	
	private ViewerFrame(){
		super("Image Viewer");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private void setFrameLayout(){
		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		setSize(screenSize.width/2, screenSize.height/2);
		setLocation(screenSize.width/4, screenSize.height/4);
		JImagePanel pGorny=new JImagePanel();
		pGorny.setBorder(BorderFactory.createRaisedBevelBorder());
		JPanel pDolny=new JPanel();
		pDolny.setBorder(BorderFactory.createRaisedBevelBorder());
		add(pGorny,BorderLayout.CENTER);
		add(pDolny,BorderLayout.SOUTH);
		JButton loadButton=new JButton("�aduj obrazek");
		pDolny.add(loadButton);
		loadButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				pGorny.setImage();
			}
		});
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        EventQueue.invokeLater(new ViewerFrame());
	}

}
